/* 
 * File:   eglobals.h
 * Author: Gary-lenovo
 *
 * Created on April 29, 2018, 7:26 PM
 */

#ifndef EGLOBALS_H
#define	EGLOBALS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    /* types */
    typedef unsigned char   Uchar;
    typedef unsigned int    Uint;

    // flags
    // new-se, -insel,-balsel, set by ext interrupt
#ifdef GLOBAL
    union{         
#else 
    extern union{ 
#endif    
        volatile struct{            
            Uint vseup : 1;
            Uint newse : 1; //new serial encoder (volume changed)  
            Uint newinsel : 1; //new input select  (input selector  changed)
            Uint newbalsel : 1; //new ballance control ( balanced changed ))
            Uint newButton : 1; //A button has been pressed, set by CNI interrupt
            Uint gainIn6 : 1;
            Uint gainIn7 : 1;
            Uint swtse : 1;
            Uint newloadsel : 1;
            Uint swise : 1;
            Uint swvse : 1;
            Uint newgainsel : 1;
            Uint spare13 : 1;
            Uint spare14 : 1;
            Uint spare15 : 1;
            Uint spare16 : 1;
        }b;
        Uint wd;
    }iflags;     
    
enum BTN_STATE{IDLE=0, PRESSED=1, LONG_PRESS=2};

//---------------   Global Variables ----------------------
#ifdef GLOBAL
volatile char VSEval;
volatile char VEmode;//0=volume, 1=balance
volatile char INSEval; //raw value from input encoder
volatile char INSmode; //0=input select; 1=gain select
volatile char GSEval; //gain select value
volatile char GSEval_prev; //previous gain select value
volatile char balSEval;
volatile unsigned char leds[5][16];
volatile unsigned char ISE_A_prev;
volatile unsigned char ISE_B_prev;
volatile unsigned char relayz[4][8];
volatile char LSEval_R; //raw value from load select encoder R Load
volatile char LSEval_C; //raw value from load select encoder C Load
volatile char LSmode;  //0=R mode, 1=C mode
volatile unsigned char LSE_A_prev;
volatile unsigned char LSE_B_prev;
volatile unsigned char VSE_A_prev;
volatile unsigned char VSE_B_prev;

volatile unsigned char DELAY_lockout; //to use for locking out any functionality during hardware delay

//Encoder button states
volatile unsigned char SWTSE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile unsigned char SWTSE_prev;//previous state of the input 0=pressed; 1=released
volatile unsigned char SWISE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile unsigned char SWISE_prev;//previous state of the input 0=pressed; 1=released
volatile unsigned char SWVSE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile unsigned char SWVSE_prev;//previous state of the input 0=pressed; 1=released

//timer variables
volatile unsigned int SWTSE_btn_count; //ms since last change of state
volatile unsigned int SWVSE_btn_count; //ms since last change of state
volatile unsigned int SWISE_btn_count; //ms since last change of state

volatile unsigned char SWTSE_tmr;//state of timer for the button 1=active 0=inactive
volatile unsigned char SWVSE_tmr;//state of timer for the button 1=active 0=inactive
volatile unsigned char SWISE_tmr;//state of timer for the button 1=active 0=inactive

// ----------------------  External Variables -------------------------
#else
volatile extern char VSEval;
volatile extern char VEmode;//0=volume, 1=balance
volatile extern char INSEval;
volatile extern char INSmode; //0=input select; 1=gain select
volatile extern char GSEval; //gain select value
volatile extern char GSEval_prev; //previous gain select value
volatile extern char balSEval;
volatile extern unsigned char leds[5][16];
volatile extern unsigned char ISE_A_prev;
volatile extern unsigned char ISE_B_prev;
volatile extern unsigned char relayz[4][8];
volatile extern char LSEval_R; //raw value from load select encoder R Load
volatile extern char LSEval_C; //raw value from load select encoder C Load
volatile extern char LSmode;  //0=R mode, 1=C mode
volatile extern unsigned char LSE_A_prev;
volatile extern unsigned char LSE_B_prev;
volatile extern unsigned char VSE_A_prev;
volatile extern unsigned char VSE_B_prev;

volatile extern unsigned char DELAY_lockout; //to use for locking out any functionality during hardware delay

//Encoder button states
volatile extern unsigned char SWTSE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile extern unsigned char SWTSE_prev;//previous state of the input 0=pressed; 1=released
volatile extern unsigned char SWISE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile extern unsigned char SWISE_prev;//previous state of the input 0=pressed; 1=released
volatile extern unsigned char SWVSE_btnState;//"official" state of the button; 0 = idle; 1 = pressed; 2= long press
volatile extern unsigned char SWVSE_prev;//previous state of the input 0=pressed; 1=released


//timer variables
volatile extern unsigned int SWTSE_btn_count; //ms since last change of state
volatile extern unsigned int SWVSE_btn_count; //ms since last change of state
volatile extern unsigned int SWISE_btn_count; //ms since last change of state

volatile extern unsigned char SWTSE_tmr;//state of timer for the button 1=active 0=inactive
volatile extern unsigned char SWVSE_tmr;//state of timer for the button 1=active 0=inactive
volatile extern unsigned char SWISE_tmr;//state of timer for the button 1=active 0=inactive

#endif // end of global Variables

#ifdef	__cplusplus
}
#endif
    
#endif	/* EGLOBALS_H */