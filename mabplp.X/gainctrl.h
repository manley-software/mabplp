#ifndef GAINCTRL_H
#define	GAINCTRL_H

#ifdef	__cplusplus
extern "C" {
#endif

    void newGencoder(void);
    void setGainRelays(char lagn, char phg50, char phg55, char phg60, char phg65, char linepho1, char linepho2, char recgn);
    void setGainLEDs(void);


#ifdef	__cplusplus
}
#endif

#endif	/* GAINCTRL_H */

