/* 
 * File:   bctl.h
 * Author: Gary-lenovo
 *
 * Created on May 14, 2018, 7:36 PM
 */

#ifndef BCTL_H
#define	BCTL_H

#ifdef	__cplusplus
extern "C" {
#endif

    void newBencoder(void);
    void setBalanceLEDs(void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* BCTL_H */

