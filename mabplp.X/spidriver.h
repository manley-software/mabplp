/* 
 * File:   spidriver.h
 * Author: Gary-lenovo
 *
 * Created on May 2, 2018, 2:37 PM
 */

#ifndef SPIDRIVER_H
#define	SPIDRIVER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    void sendleds(void);
    void sendRelays(void);
    void sendRelaysVolBal(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SPIDRIVER_H */

