// file sctl.c  input Selector control
// Device header file
#if defined(__XC16__)
   #include <xc.h>

#endif    
#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "loadctrl.h"
#include "mcc_generated_files/tmr1.h"
#include "controls.h"

#define LOADDELAY 2

void setCloadRelays(int K7, int K8, int K9, int K10){
    RLY_K7 = K7;
    RLY_K8 = K8;
    RLY_K9 = K9;
    RLY_K10 = K10;
    sendRelays();
}

void setRloadRelays(char K2, char K3, char K4, char K5, char K6, char K1){
    RLY_K2 = K2;
    RLY_K3 = K3;
    RLY_K4 = K4;
    RLY_K5 = K5;
    RLY_K6 = K6;
    RLY_K1 = K1;
    sendRelays();
}

void setWhiteLoadLEDS(char val){
    LED17W = (val == 32) || (val > 1 && val < 17); //LED1
    LED18W = (val == 32) || (val > 2 && val < 18); //LED2
    LED19W = (val == 32) || (val > 3 && val < 19); //LED3
    LED20W = (val > 31 && val <= 33) || (val > 4 && val < 20); //LED4
    LED21W = (val > 31 && val <= 33) || (val > 5 && val < 21); //LED5
    LED22W = (val > 31 && val <= 33) || (val > 6 && val < 22); //LED6
    LED23W = (val > 31 && val <= 33) || (val > 7 && val < 23); //LED7
    LED24W = (val > 31 && val <= 33) || (val > 8 && val < 24); //LED8
    LED25W = (val > 31 && val <= 33) || (val > 9 && val < 25); //LED9
    LED26W = (val > 31 && val <= 33) || (val > 10 && val < 26); //LED10
    LED27W = (val > 31 && val <= 33) || (val > 11 && val < 27); //LED11
    LED28W = (val > 31 && val <= 33) || (val > 12 && val < 28); //LED12
    LED29W = (val > 31 && val <= 33) || (val > 13 && val < 29); //LED13
    LED30W = (val == 32) || (val > 14 && val < 30); //LED14
    LED31W = (val == 32) || (val > 15 && val < 31); //LED15
    LED32W = (val > 16 && val < 33); //LED16
    
    LED17R = 0;
    LED18R = 0;
    LED19R = 0;
    LED20R = 0;
    LED21R = 0;
    LED22R = 0;
    LED23R = 0;
    LED24R = 0;
    LED25R = 0;
    LED26R = 0;
    LED27R = 0;
    LED28R = 0;
    LED29R = 0;
    LED30R = 0;
    LED31R = 0;
    LED32R = 0;
    
    sendleds();
}

void setRedLoadLEDS(char val){
    LED17R = (val > 0); //LED1
    LED18R = (val > 1); //LED2
    LED19R = (val > 2); //LED3
    LED20R = (val > 3); //LED4
    LED21R = (val > 4); //LED5
    LED22R = (val > 5); //LED6
    LED23R = (val > 6); //LED7
    LED24R = (val > 7); //LED8
    LED25R = (val > 8); //LED9
    LED26R = (val > 9); //LED10
    LED27R = (val > 10); //LED11
    LED28R = (val > 11); //LED12
    LED29R = (val > 12); //LED13
    LED30R = (val > 13); //LED14
    LED31R = (val > 14); //LED15
    LED32R = (val > 15); //LED16
    
    LED17W = 0;
    LED18W = 0;
    LED19W = 0;
    LED20W = 0;
    LED21W = 0;
    LED22W = 0;
    LED23W = 0;
    LED24W = 0;
    LED25W = 0;
    LED26W = 0;
    LED27W = 0;
    LED28W = 0;
    LED29W = 0;
    LED30W = 0;
    LED31W = 0;
    LED32W = 0;
    
    sendleds();
}

void resetLoadLEDs(){
    LED17W = 0;
    LED18W = 0;
    LED19W = 0;
    LED20W = 0;
    LED21W = 0;
    LED22W = 0;
    LED23W = 0;
    LED24W = 0;
    LED25W = 0;
    LED26W = 0;
    LED27W = 0;
    LED28W = 0;
    LED29W = 0;
    LED30W = 0;
    LED31W = 0;
    LED32W = 0;
    LED17R = 0;
    LED18R = 0;
    LED19R = 0;
    LED20R = 0;
    LED21R = 0;
    LED22R = 0;
    LED23R = 0;
    LED24R = 0;
    LED25R = 0;
    LED26R = 0;
    LED27R = 0;
    LED28R = 0;
    LED29R = 0;
    LED30R = 0;
    LED31R = 0;
    LED32R = 0;
}

/*
 * Load selection. Relays are normally closed
 */
void newLencoder(void){
    TMR1_SoftwareCounterClear();
    if(LSmode){ //C Load mode = 1; R Load mode = 0
        while(TMR1_SoftwareCounterGet() < LOADDELAY){
            setRedLoadLEDS(LSEval_C);
        }
        switch(LSEval_C){
            case 1:
                setCloadRelays(0, 0, 0, 0);
                resetLoadLEDs();
                LED17R = 1;
                break;
            case 2:
                setCloadRelays(1, 0, 0, 0);
                resetLoadLEDs();
                LED18R = 1;
                break;
            case 3:
                setCloadRelays(0, 1, 0, 0);
                resetLoadLEDs();
                LED19R = 1;
                break;
            case 4:
                setCloadRelays(1, 1, 0, 0);
                resetLoadLEDs();
                LED20R = 1;
                break;
            case 5:
                setCloadRelays(0, 0, 1, 0);
                resetLoadLEDs();
                LED21R = 1;
                break;
            case 6:
                setCloadRelays(1, 0, 1, 0);
                resetLoadLEDs();
                LED22R = 1;
                break;
            case 7:
                setCloadRelays(0, 1, 1, 0);
                resetLoadLEDs();
                LED23R = 1;
                break;
            case 8:
                setCloadRelays(1, 1, 1, 0);
                resetLoadLEDs();
                LED24R = 1;
                break;
            case 9:
                setCloadRelays(0, 0, 0, 1);
                resetLoadLEDs();
                LED25R = 1;
                break;
            case 10:
                setCloadRelays(1, 0, 0, 1);
                resetLoadLEDs();
                LED26R = 1;
                break;
            case 11:
                setCloadRelays(0, 1, 0, 1);
                resetLoadLEDs();
                LED27R = 1;
                break;
            case 12:
                setCloadRelays(1, 1, 0, 1);
                resetLoadLEDs();
                LED28R = 1;
                break;
            case 13:
                setCloadRelays(0, 0, 1, 1);
                resetLoadLEDs();
                LED29R = 1;
                break;
            case 14:
                setCloadRelays(1, 0, 1, 1);
                resetLoadLEDs();
                LED30R = 1;
                break;
            case 15:
                setCloadRelays(0, 1, 1, 1);
                resetLoadLEDs();
                LED31R = 1;
                break;
            case 16:
                setCloadRelays(1, 1, 1, 1);
                resetLoadLEDs();
                LED32R = 1;
                break;
        }
    }else{
        while(TMR1_SoftwareCounterGet() < LOADDELAY){
            setWhiteLoadLEDS(LSEval_R);
        }
        switch(LSEval_R){
            case 1:
                setRloadRelays(1, 1, 1, 1, 1, 1);
                break;
            case 2:
                setRloadRelays(0, 1, 1, 1, 1, 1);
                break;
            case 3:
                setRloadRelays(1, 0, 1, 1, 1, 1);
                break;
            case 4:
                setRloadRelays(0, 0, 1, 1, 1, 1);
                break;
            case 5:
                setRloadRelays(1, 1, 0, 1, 1, 1);
                break;
            case 6:
                setRloadRelays(0, 1, 0, 1, 1, 1);
                break;
            case 7:
                setRloadRelays(1, 0, 0, 1, 1, 1);
                break;
            case 8:
                setRloadRelays(0, 0, 0, 1, 1, 1);
                break;
            case 9:
                setRloadRelays(1, 1, 1, 0, 1, 1);
                break;
            case 10:
                setRloadRelays(0, 1, 1, 0, 1, 1);
                break;
            case 11:
                setRloadRelays(1, 0, 1, 0, 1, 1);
                break;
            case 12:
                setRloadRelays(0, 0, 1, 0, 1, 1);
                break;
            case 13:
                setRloadRelays(1, 1, 0, 0, 1, 1);
                break;
            case 14:
                setRloadRelays(0, 1, 0, 0, 1, 1);
                break;
            case 15:
                setRloadRelays(1, 0, 0, 0, 1, 1);
                break;
            case 16:
                setRloadRelays(0, 0, 0, 0, 1, 1);
                break;
            case 17:
                setRloadRelays(1, 1, 1, 1, 0, 1);
                break;
            case 18:
                setRloadRelays(0, 1, 1, 1, 0, 1);
                break;
            case 19:
                setRloadRelays(1, 0, 1, 1, 0, 1);
                break;
            case 20:
                setRloadRelays(0, 0, 1, 1, 0, 1);
                break;
            case 21:
                setRloadRelays(1, 1, 0, 1, 0, 1);
                break;
            case 22:
                setRloadRelays(0, 1, 0, 1, 0, 1);
                break;
            case 23:
                setRloadRelays(1, 0, 0, 1, 0, 1);
                break;
            case 24:
                setRloadRelays(0, 0, 0, 1, 0, 1);
                break;
            case 25:
                setRloadRelays(1, 1, 1, 0, 0, 1);
                break;
            case 26:
                setRloadRelays(0, 1, 1, 0, 0, 1);
                break;
            case 27:
                setRloadRelays(1, 0, 1, 0, 0, 1);
                break;
            case 28:
                setRloadRelays(0, 0, 1, 0, 0, 1);
                break;
            case 29:
                setRloadRelays(1, 1, 0, 0, 0, 1);
                break;
            case 30:
                setRloadRelays(0, 1, 0, 0, 0, 1);
                break;
            case 31:
                setRloadRelays(1, 0, 0, 0, 0, 1);
                break;
            case 32:
                setRloadRelays(0, 0, 0, 0, 0, 1);
                break;
            case 33:
                setRloadRelays(0, 0, 0, 0, 0, 0);
                break;
        }
    }
}
