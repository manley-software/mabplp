/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:  MLPA macros.h
 * Author: Gary
 * Comments: LEDs SPI mode 0
 * Revision history: 
 */

/*
 * This is a guard condition so that contents of this file are not included more than once.
 */
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

// include processor files - each processor file is guarded.
#include <xc.h>   

#define SPI1SS      PORTGbits.RG8      // SPI strobe, high loads data
#define SPI2SS      PORTBbits.RB10
#define SPI3SS      PORTGbits.RG9      // SPI3 strobe

//Volume encoder
#define VSE_A       PORTDbits.RD10      // EXT_INT 1
#define VSE_B       PORTDbits.RD11      // EXT_INT 2
#define SWVSE       !PORTDbits.RD6       //Pushbutton Switch (normally true)

// Input Select Encoder
#define ISE_A       PORTDbits.RD1       
#define ISE_B       PORTDbits.RD0
#define SWISE       !PORTDbits.RD4       //Pushbutton Switch, In Select, Encdr. (normally true)

// Termination Select Encoder
#define LSE_A       PORTDbits.RD2       
#define LSE_B       PORTDbits.RD3
#define SWTSE       !PORTDbits.RD5       //Pushbutton Switch, Term Select, Encdr. (normally true)

// timers
#define FOSC (8000000UL)
#define FCY (FOSC/2)

/*
 * LED Driver Pin Assignment macros
 * leds[Un-1][PIN#] where Un is the LED driver component label indexed starting at 0 so U1 = leds[0][0..15]
 */
#define LED39 leds[0][12]
#define LED38R leds[0][11]
#define LED37R leds[0][10]
#define LED36R leds[0][9]
#define LED35R leds[0][8]
#define LED34R leds[0][7]
#define LED33R leds[0][6]
#define LED38W leds[0][5]
#define LED37W leds[0][4]
#define LED36W leds[0][3]
#define LED35W leds[0][2]
#define LED34W leds[0][1]
#define LED33W leds[0][0]
#define LED48W leds[1][9]
#define LED48R leds[1][8]
#define LED46 leds[1][6]
#define LED45 leds[1][5]
#define LED44 leds[1][4]
#define LED43 leds[1][3]
#define LED42 leds[1][2]
#define LED41 leds[1][1]
#define LED40 leds[1][0]
#define LED16 leds[2][15]
#define LED15 leds[2][14]
#define LED14 leds[2][13]
#define LED13 leds[2][12]
#define LED12 leds[2][11]
#define LED11 leds[2][10]
#define LED10 leds[2][9]
#define LED9 leds[2][8]
#define LED8 leds[2][7]
#define LED7 leds[2][6]
#define LED6 leds[2][5]
#define LED5 leds[2][4]
#define LED4 leds[2][3]
#define LED3 leds[2][2]
#define LED2 leds[2][1]
#define LED1 leds[2][0]
#define LED32W leds[3][15]
#define LED31W leds[3][14]
#define LED30W leds[3][13]
#define LED29W leds[3][12]
#define LED28W leds[3][11]
#define LED27W leds[3][10]
#define LED26W leds[3][9]
#define LED25W leds[3][8]
#define LED24W leds[3][7]
#define LED23W leds[3][6]
#define LED22W leds[3][5]
#define LED21W leds[3][4]
#define LED20W leds[3][3]
#define LED19W leds[3][2]
#define LED18W leds[3][1]
#define LED17W leds[3][0]
#define LED32R leds[4][15]
#define LED31R leds[4][14]
#define LED30R leds[4][13]
#define LED29R leds[4][12]
#define LED28R leds[4][11]
#define LED27R leds[4][10]
#define LED26R leds[4][9]
#define LED25R leds[4][8]
#define LED24R leds[4][7]
#define LED23R leds[4][6]
#define LED22R leds[4][5]
#define LED21R leds[4][4]
#define LED20R leds[4][3]
#define LED19R leds[4][2]
#define LED18R leds[4][1]
#define LED17R leds[4][0]

/*
 * Relay Driver Pin Assignment macros
 * relayz[Un-1][OUT#-1] where Un is the relay driver component label indexed starting at 0 so U1 = relayz[0][0..7]
 */
#define RLY_IN1 relayz[0][0] //OUT1
#define RLY_IN2 relayz[0][1]
#define RLY_IN3 relayz[0][2]
#define RLY_IN4 relayz[0][3]
#define RLY_IN5 relayz[0][4]
#define RLY_IN6 relayz[0][5]
#define RLY_PH_SEL relayz[0][7] //OUT8
#define RLY_RECMUTE relayz[1][3]
#define RLY_MUTE relayz[1][4]
#define RLY_HTBYP relayz[1][6]

#define RLY_K2 relayz[2][0] //TR30
#define RLY_K3 relayz[2][1] //TR60
#define RLY_K4 relayz[2][2] //TR120
#define RLY_K5 relayz[2][3] //TR240
#define RLY_K6 relayz[2][4] //TR480
#define RLY_K1 relayz[2][5] //TR30
#define RLY_K7 relayz[3][0] //C30
#define RLY_K8 relayz[3][1] //C60
#define RLY_K9 relayz[3][2] //C120
#define RLY_K10 relayz[3][3] //C240

/*
 * Main board GPIO Relays
 */
#define RLY_PHG50 PORTEbits.RE3 //AU1-K8(L)&K12(R)
#define RLY_PHG55 PORTEbits.RE4 //AU1-K9(L)&K13(R)
#define RLY_PHG60 PORTEbits.RE5 //AU1-K10(L)&K14(R)
#define RLY_PHG65 PORTEbits.RE6 //AU1-K11(L)&K15(R)
#define RLY_RECGN PORTEbits.RE7 //AU1-K4
#define RLY_LINEPHO1 PORTFbits.RF0 //AU1-K1(L)&K5(R)
#define RLY_LINEPHO2 PORTFbits.RF1 //AU2-K2(L)&K6(R)
#define RLY_LAGN PORTGbits.RG6 //AU1-K3(L)&K7(R)

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

