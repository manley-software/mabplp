/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC24 / dsPIC33 / PIC32MM MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : 1.53.0.1
        Device            :  PIC24FJ64GA106
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.33
        MPLAB             :  MPLAB X v4.05
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "controls.h"
#include "spidriver.h"
#include "volctrl.h"
#include "loadctrl.h"
#include "balctrl.h"
#include "inputctrl.h"
#include "gainctrl.h"

#define GLOBAL
#include "globals.h"

#define BTN_SHORTPRESS_MS 275//how long the button needs to be stable to be considered "pressed momentarily"
#define BTN_LONGPRESS_MS 3000//how long the button needs to be stabel to be considered "pressed and held"
//#define BTN_RELEASE_MS 100//how long the button needs to be stable to be considered "released"
//#define BTN_CHECK_MS 5 //how often the buttons will be checked for changes of state



extern void init(void);
void blinkSWISELEDS(void);

int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    
    //Initialize
    init();

    while(1){
        /**********************  STATE MACHINE***********************/
        // if encoder changed
        if(iflags.b.newse){
            iflags.b.newse = 0;
            newVencoder();
        }
        
        // if input selector changed
        if(iflags.b.newinsel){
            newIencoder();// new input selected, go and service
            iflags.b.newinsel = 0; //reset flag
        }
        
        if(iflags.b.newbalsel){
            newBencoder();
            iflags.b.newbalsel = 0;
        }
        
        if(iflags.b.newgainsel){
            if(GSEval != GSEval_prev)
                newGencoder();
            iflags.b.newgainsel = 0;
        }
        
        if(iflags.b.newloadsel){
            newLencoder();// new input selected, go and service
            iflags.b.newloadsel = 0; //reset flag
        }
        
        //button handling
        if(iflags.b.swtse){
            handleSWTSE();
            iflags.b.swtse = 0;
        }
        if(iflags.b.swise){
            handleSWISE();
            iflags.b.swise = 0;
        }
        if(iflags.b.swvse){
            handleSWVSE();
            iflags.b.swvse = 0;
        }
        
        //Termination Encoder button state
        if(SWTSE_tmr == 1){ //if the timer is active
            if(SWTSE_btn_count >= BTN_LONGPRESS_MS){
                SWTSE_btnState = LONG_PRESS;
            }
        }else{ //if the timer is inactive
            //button short cycle
            if(SWTSE_btn_count < BTN_SHORTPRESS_MS && SWTSE_btn_count > 0){
                SWTSE_btnState = IDLE;
            }else if(SWTSE_btn_count >= BTN_SHORTPRESS_MS && SWTSE_btn_count < BTN_LONGPRESS_MS){
                SWTSE_btnState = PRESSED;
            }else if(SWTSE_btn_count >= BTN_LONGPRESS_MS){
                SWTSE_btnState = IDLE;
            }
            //reset the count only if it has a value
            if(SWTSE_btn_count > 0){
                SWTSE_btn_count = 0;
            }
        }
        //R&C Load mode evaluation
        if(!LSmode && SWTSE_btnState > 0){ //in R LOAD mode and button pressed
            LSmode = 1; //set C LOAD mode
            SWTSE_btnState = IDLE;
            setRedLoadLEDS(LSEval_C);
        }else if(LSmode && SWTSE_btnState > 0){ //in C LOAD mode and button pressed
            LSmode = 0; //set R LOAD mode
            SWTSE_btnState = IDLE;
            setWhiteLoadLEDS(LSEval_R);
        }
        
        
        //Volume Encoder button state
        if(SWVSE_tmr == 1){ //if the timer is active
            if(SWVSE_btn_count >= BTN_LONGPRESS_MS){
                SWVSE_btnState = LONG_PRESS;
            }
        }else{ //if the timer is inactive
            //button short cycle
            if(SWVSE_btn_count < BTN_SHORTPRESS_MS && SWVSE_btn_count > 0){
                SWVSE_btnState = IDLE;
            }else if(SWVSE_btn_count >= BTN_SHORTPRESS_MS && SWVSE_btn_count < BTN_LONGPRESS_MS){
                SWVSE_btnState = PRESSED;
            }else if(SWVSE_btn_count >= BTN_LONGPRESS_MS){
                SWVSE_btnState = IDLE;
            }
            //reset the count only if it has a value
            if(SWVSE_btn_count > 0){
                SWVSE_btn_count = 0;
            }
        }
        //volume and balance mode evaluation
        if(!VEmode && SWVSE_btnState > 0){ //in volume mode and button long or short press
                VEmode = 1; //set balance mode
                SWVSE_btnState = IDLE;
                setBalanceLEDs();
        }else if(VEmode && SWVSE_btnState > 0){
            VEmode = 0; //set volume mode
            SWVSE_btnState = IDLE;
            setVolumeLEDs();
        }
        
        //INPUT Encoder button state
        if(SWISE_tmr == 1){ //if the timer is active
            if(SWISE_btn_count >= BTN_LONGPRESS_MS){
                SWISE_btnState = LONG_PRESS;
            }
        }else{ //if the timer is inactive
            //button short cycle
            if(SWISE_btn_count < BTN_SHORTPRESS_MS && SWISE_btn_count > 0){
                SWISE_btnState = IDLE;
            }else if(SWISE_btn_count >= BTN_SHORTPRESS_MS && SWISE_btn_count < BTN_LONGPRESS_MS){
                SWISE_btnState = PRESSED;
            }else if(SWISE_btn_count >= BTN_LONGPRESS_MS){
                SWISE_btnState = IDLE;
            }
            //reset the count only if it has a value
            if(SWISE_btn_count > 0){
                SWISE_btn_count = 0;
            }
        }
        //Input & Gain mode evaluation
        if(!INSmode && SWISE_btnState > 0){ //in input select mode and button pressed
            INSmode = 1; //set gain select mode
            SWISE_btnState = IDLE;
            blinkSWISELEDS();
        }else if(INSmode && SWISE_btnState > 0){ //in gain select mode and button pressed
            INSmode = 0; //set input select mode
            SWISE_btnState = IDLE;
            blinkSWISELEDS();
        }
    }
    
    return -1;  
 }

void blinkSWISELEDS(void){
    //store gain select LED state for blinking
    char led40 = LED40;
    char led41 = LED41;
    char led42 = LED42;
    char led43 = LED43;
    char led44 = LED44;
    char led45 = LED45;
    char led46 = LED46;
    
    LED40 = 0;
    LED41 = 0;
    LED42 = 0;
    LED43 = 0;
    LED44 = 0;
    LED45 = 0;
    LED46 = 0;
    
    LED33R = 0; //input 1 LED
    LED34R = 0; //input 2 LED
    LED35R = 0; //input 3 LED
    LED36R = 0; //input 4 LED
    LED37R = 0; //input 5 LED
    LED38R = 0; //input 6 LED
    sendleds();
    
    hardDelay(100);
    
    LED33R = (INSEval == 1); //input 1 LED
    LED34R = (INSEval == 2); //input 2 LED
    LED35R = (INSEval == 3); //input 3 LED
    LED36R = (INSEval == 4); //input 4 LED
    LED37R = (INSEval == 5); //input 5 LED
    LED38R = (INSEval == 6); //input 6 LED
    
    LED40 = led40;
    LED41 = led41;
    LED42 = led42;
    LED43 = led43;
    LED44 = led44;
    LED45 = led45;
    LED46 = led46;
    sendleds();
    
    LED33R = 0; //input 1 LED
    LED34R = 0; //input 2 LED
    LED35R = 0; //input 3 LED
    LED36R = 0; //input 4 LED
    LED37R = 0; //input 5 LED
    LED38R = 0; //input 6 LED
    
    LED40 = 0;
    LED41 = 0;
    LED42 = 0;
    LED43 = 0;
    LED44 = 0;
    LED45 = 0;
    LED46 = 0;
    sendleds();
    
    hardDelay(100);
    
    LED33R = (INSEval == 1); //input 1 LED
    LED34R = (INSEval == 2); //input 2 LED
    LED35R = (INSEval == 3); //input 3 LED
    LED36R = (INSEval == 4); //input 4 LED
    LED37R = (INSEval == 5); //input 5 LED
    LED38R = (INSEval == 6); //input 6 LED
    
    LED40 = led40;
    LED41 = led41;
    LED42 = led42;
    LED43 = led43;
    LED44 = led44;
    LED45 = led45;
    LED46 = led46;
    sendleds();
}