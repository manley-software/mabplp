/* 
 * File:   sctl.h
 * Author: Gary-lenovo
 *
 * Created on May 3, 2018, 8:19 PM
 */

#ifndef SCTL_H
#define	SCTL_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    void newLencoder(void);
    void setWhiteLoadLEDS(char val);
    void setRedLoadLEDS(char val);

#ifdef	__cplusplus
}
#endif

#endif	/* SCTL_H */

