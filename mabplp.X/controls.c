#if defined(__XC16__)
   #include <xc.h>
#endif

#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "controls.h"
#include <libpic30.h>

void handleSWTSE(void){
    //start and stop the timer when the button is pressed or released
    //previous raw state of the button 0 for released 1 for pressed
    if(SWTSE_prev){
        SWTSE_tmr = 1;
    }else{
        SWTSE_tmr = 0;
    }
}

void handleSWISE(void){
    //start and stop the timer when the button is pressed or released
    //previous raw state of the button 0 for released 1 for pressed
    if(SWISE_prev){
        SWISE_tmr = 1;
    }else{
        SWISE_tmr = 0;
    }
}

void handleSWVSE(void){
    //start and stop the timer when the button is pressed or released
    //previous raw state of the button 0 for released 1 for pressed
    if(SWVSE_prev){
        SWVSE_tmr = 1;
    }else{
        SWVSE_tmr = 0;
    }
}

void processEncoder(int prev, int cur, int min, int max, volatile char* val){
    switch(cur){
        case 0:
            if(prev == 1 && (*val) < max)
                (*val)++;
            else if(prev == 2 && (*val) > min)
                (*val)--;
            break;
        case 1:
            if(prev == 0 && (*val) > min)
                (*val)--;
            else if(prev == 3 && (*val) < max)
                (*val)++;
            break;
        case 2:
            if(prev == 0 && (*val) < max)
                (*val)++;
            else if(prev == 3 && (*val) > min)
                (*val)--;
            break;
        case 3:
            if(prev == 1 && (*val) > min)
                (*val)--;
            else if(prev == 2 && (*val) < max)
                (*val)++;
            break;
    }
}

void hardDelay(unsigned int ms){
    DELAY_lockout = 1;
    __delay_ms(ms);
    DELAY_lockout = 0;
}