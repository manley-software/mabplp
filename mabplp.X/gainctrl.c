#if defined(__XC16__)
   #include <xc.h>
#endif

#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "controls.h"
#include "gainctrl.h"

void newGencoder(void){
    setGainLEDs();
    
    //mute output relays
    RLY_RECMUTE = 0;
    RLY_MUTE = 0;
    sendRelays();
    
    //set volume to 0
    unsigned char VSEval_old = VSEval;
    VSEval = 0;
    sendRelaysVolBal();
    
    //set gain relays
    switch(GSEval){
        case 1:
            setGainRelays(0,0,0,0,0,0,0,0);
            RLY_PH_SEL = 0; //line only mode
            break;
        case 2:
            setGainRelays(1,0,0,0,0,0,0,1);
            RLY_PH_SEL = 0; //line only mode
            break;
        case 3:
            setGainRelays(0,0,0,0,0,0,1,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 4:
            setGainRelays(0,1,0,0,0,0,1,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 5:
            setGainRelays(0,1,1,0,0,0,1,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 6:
            setGainRelays(0,1,1,1,0,0,1,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 7:
            setGainRelays(0,1,1,1,1,0,1,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 8:
            setGainRelays(0,0,0,0,0,1,0,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 9:
            setGainRelays(0,1,0,0,0,1,0,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 10:
            setGainRelays(1,0,0,0,0,1,0,1);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 11:
            setGainRelays(0,1,1,0,0,1,0,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 12:
            setGainRelays(1,1,0,0,0,1,0,1);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 13:
            setGainRelays(0,1,1,1,0,1,0,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 14:
            setGainRelays(1,1,1,0,0,1,0,1);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 15:
            setGainRelays(0,1,1,1,1,1,0,0);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 16:
            setGainRelays(1,1,1,1,0,1,0,1);
            RLY_PH_SEL = 1; //phono  mode
            break;
        case 17:
            setGainRelays(1,1,1,1,1,1,0,1);
            RLY_PH_SEL = 1; //phono  mode
            break;
    }
    
    sendRelays();
    
    hardDelay(5000);
    
    //reset volume back
    VSEval = VSEval_old;
    sendRelaysVolBal();
    
    hardDelay(5000);
    
    //unmute output relays
    RLY_RECMUTE = 1;
    RLY_MUTE = 1;
    sendRelays();
}

void setGainRelays(char lagn, char phg50, char phg55, char phg60, char phg65, char linepho1, char linepho2, char recgn){
    RLY_PHG50 = phg50;
    RLY_PHG55 = phg55;
    RLY_PHG60 = phg60;
    RLY_PHG65 = phg65;
    
    hardDelay(5000);
    
    RLY_RECGN = recgn;
    RLY_LAGN = lagn;
    RLY_LINEPHO1 = linepho1;
    RLY_LINEPHO2 = linepho2;
    hardDelay(5000);
}

void setGainLEDs(void){
    LED40 = 0;
    LED41 = 0;
    LED42 = 0;
    LED43 = 0;
    LED44 = 0;
    LED45 = 0;
    LED46 = 0;
    switch(GSEval){
        case 1:
            LED40 = 1;
            break;
        case 2:
            LED41 = 1;
            break;
        case 3:
            LED42 = 1;
            break;
        case 4:
            LED43 = 1;
            break;
        case 5:
            LED44 = 1;
            break;
        case 6:
            LED45 = 1;
            break;
        case 7:
            LED46 = 1;
            break;
        case 8:
            LED40 = 1;
            LED42 = 1;
            break;
        case 9:
            LED40 = 1;
            LED43 = 1;
            break;
        case 10:
            LED41 = 1;
            LED42 = 1;
            break;
        case 11:
            LED40 = 1;
            LED44 = 1;
            break;
        case 12:
            LED41 = 1;
            LED43 = 1;
            break;
        case 13:
            LED40 = 1;
            LED45 = 1;
            break;
        case 14:
            LED41 = 1;
            LED44 = 1;
            break;
        case 15:
            LED40 = 1;
            LED46 = 1;
            break;
        case 16:
            LED41 = 1;
            LED45 = 1;
            break;
        case 17:
            LED41 = 1;
            LED46 = 1;
            break;
    }
    sendleds();
}
