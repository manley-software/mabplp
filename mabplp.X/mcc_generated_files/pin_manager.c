/**
  System Interrupts Generated Driver File 

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.c

  @Summary:
    This is the generated manager file for the MPLAB(c) Code Configurator device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description:
    This source file provides implementations for MPLAB(c) Code Configurator interrupts.
    Generation Information : 
        Product Revision  :  MPLAB(c) Code Configurator - 4.45.9
        Device            :  PIC24FJ64GA106
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.33
        MPLAB             :  MPLAB X v4.05

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

    Microchip licenses to you the right to use, modify, copy and distribute
    Software only when embedded on a Microchip microcontroller or digital signal
    controller that is integrated into your product or third party product
    (pursuant to the sublicense terms in the accompanying license agreement).

    You should refer to the license agreement accompanying this Software for
    additional information regarding your rights and obligations.

    SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
    EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
    MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
    IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
    CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
    OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
    INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
    CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
    SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
    (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

*/


/**
    Section: Includes
*/
#include <xc.h>
#include "pin_manager.h"
#include "../macros.h"
#include "../globals.h"
#include "../controls.h"

//void processEncoder(int prev, int cur, int min, int max, volatile char* val);

/**
    void PIN_MANAGER_Initialize(void)
*/
void PIN_MANAGER_Initialize(void)
{
    /****************************************************************************
     * Setting the Output Latch SFR(s)
     ***************************************************************************/
    LATB = 0x0000;
    LATC = 0x0000;
    LATD = 0x0000;
    LATE = 0x0010;
    LATF = 0x0000;
    LATG = 0x0000;

    /****************************************************************************
     * Setting the GPIO Direction SFR(s)
     ***************************************************************************/
    TRISB = 0xB934;
    TRISC = 0x9000;
    TRISD = 0x0DFF;
    TRISE = 0x0000;
    TRISF = 0x0010;
    TRISG = 0x008C;

    /****************************************************************************
     * Setting the Weak Pull Up and Weak Pull Down SFR(s)
     ***************************************************************************/
    CNPD1 = 0x0000;
    CNPD2 = 0x0000;
    CNPD3 = 0x0000;
    CNPD4 = 0x0000;
    CNPD5 = 0x0000;
    CNPD6 = 0x0000;
    CNPU1 = 0xE000;
    CNPU2 = 0x0000;
    CNPU3 = 0x0000;
    CNPU4 = 0x0006;
    CNPU5 = 0x0000;
    CNPU6 = 0x0000;

    /****************************************************************************
     * Setting the Open Drain SFR(s)
     ***************************************************************************/
    ODCB = 0x0000;
    ODCC = 0x0000;
    ODCD = 0x0000;
    ODCE = 0x0002;
    ODCF = 0x0000;
    ODCG = 0x0000;

    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR(s)
     ***************************************************************************/
    AD1PCFGH = 0x0000;
    AD1PCFGL = 0x8134;


    /****************************************************************************
     * Set the PPS
     ***************************************************************************/
    __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS

    RPINR0bits.INT1R = 0x0003;   //RD10->EXT_INT:INT1;
    RPOR8bits.RP16R = 0x0008;   //RF3->SPI1:SCK1OUT;
    RPOR8bits.RP17R = 0x0005;   //RF5->UART2:U2TX;
    RPOR7bits.RP14R = 0x0004;   //RB14->UART1:U1RTS;
    RPINR1bits.INT2R = 0x000C;   //RD11->EXT_INT:INT2;
    RPOR4bits.RP9R = 0x0003;   //RB9->UART1:U1TX;
    RPINR28bits.SDI3R = 0x000D;   //RB2->SPI3:SDI3;
    RPOR15bits.RP30R = 0x0007;   //RF2->SPI1:SDO1;
    RPINR19bits.U2RXR = 0x000A;   //RF4->UART2:U2RX;
    RPOR3bits.RP6R = 0x000A;   //RB6->SPI2:SDO2;
    RPINR18bits.U1CTSR = 0x001D;   //RB15->UART1:U1CTS;
    RPOR3bits.RP7R = 0x000B;   //RB7->SPI2:SCK2OUT;
    RPINR18bits.U1RXR = 0x0008;   //RB8->UART1:U1RX;
    RPOR0bits.RP0R = 0x0020;   //RB0->SPI3:SDO3;
    RPOR9bits.RP19R = 0x0009;   //RG8->SPI1:SS1OUT;
    RPOR0bits.RP1R = 0x0021;   //RB1->SPI3:SCK3OUT;

    __builtin_write_OSCCONL(OSCCON | 0x40); // lock   PPS

    /****************************************************************************
     * Interrupt On Change for group CNEN1 - any
     ***************************************************************************/
	CNEN1bits.CN13IE = 1; // Pin : RD4
	CNEN1bits.CN14IE = 1; // Pin : RD5
	CNEN1bits.CN15IE = 1; // Pin : RD6

    /****************************************************************************
     * Interrupt On Change for group CNEN4 - any
     ***************************************************************************/
	CNEN4bits.CN49IE = 1; // Pin : RD0
	CNEN4bits.CN50IE = 1; // Pin : RD1
	CNEN4bits.CN51IE = 1; // Pin : RD2
	CNEN4bits.CN52IE = 1; // Pin : RD3

    IEC1bits.CNIE = 1; // Enable CNI interrupt 
}

/* Interrupt service routine for the CNI interrupt. */
void __attribute__ (( interrupt, no_auto_psv )) _CNInterrupt ( void )
{
    if(IFS1bits.CNIF == 1)
    {
        //handle load encoder button change of state
        if(SWTSE_prev != SWTSE && DELAY_lockout == 0){
            SWTSE_prev = SWTSE_prev ? 0 : 1;
            iflags.b.swtse = 1;
        }
        
        //handle input encoder button change of state
        if(SWISE_prev != SWISE && DELAY_lockout == 0){
            SWISE_prev = SWISE_prev ? 0 : 1;
            iflags.b.swise = 1;
        }
        
        //handle volume encoder button change of state
        if(SWVSE_prev != SWVSE && DELAY_lockout == 0){
            SWVSE_prev = SWVSE_prev ? 0 : 1;
            iflags.b.swvse = 1;
        }
        
        int Iprev = 0;
        int Icur = 0;
        if(INSmode){ //0=input select; 1=gain select
            Iprev = ISE_B_prev + ISE_A_prev*2;
            Icur = ISE_B + ISE_A*2;
        }else{
            Iprev = ISE_A_prev + ISE_B_prev*2;
            Icur = ISE_A + ISE_B*2;
        }
        
        //handle Input encoder rotation
        if(Iprev != Icur && DELAY_lockout == 0){
            if(INSmode){ //0=input select; 1=gain select
                GSEval_prev = GSEval; //store previous value before change
                processEncoder(Iprev, Icur, 1, 17, &GSEval);
                iflags.b.newgainsel = 1;
            }else{
                processEncoder(Iprev, Icur, 1, 6, &INSEval);
                iflags.b.newinsel = 1;
            }
            ISE_A_prev = ISE_A;
            ISE_B_prev = ISE_B;
        }else{
            ISE_A_prev = ISE_A;
            ISE_B_prev = ISE_B;
        }
        
        int Lprev = LSE_B_prev + LSE_A_prev*2;
        int Lcur = LSE_B + LSE_A*2;
        
        if(Lprev != Lcur && DELAY_lockout == 0){
            if(LSmode)
                processEncoder(Lprev, Lcur, 1, 16, &LSEval_C);
            else
                processEncoder(Lprev, Lcur, 1, 33, &LSEval_R);
            
            iflags.b.newloadsel = 1;
            LSE_A_prev = LSE_A;
            LSE_B_prev = LSE_B;
        }else{
            LSE_A_prev = LSE_A;
            LSE_B_prev = LSE_B;
        }

        // Clear the flag
        IFS1bits.CNIF = 0;
    }
}
