/**
  EXT_INT Generated Driver File 

  @Company:
    Microchip Technology Inc.

  @File Name:
    ext_int.c

  @Summary
    This is the generated driver implementation file for the EXT_INT 
    driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description:
    This source file provides implementations for driver APIs for EXT_INT. 
    Generation Information : 
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : 1.53.0.1
        Device            :  PIC24FJ64GA106
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.33
        MPLAB             :  MPLAB X v4.05
*/

/**
   Section: Includes
 */
#include <xc.h>
#include "ext_int.h"
//***User Area Begin->code: Add External Interrupt handler specific headers 
#include "../globals.h"  
#include "../macros.h"
#include "../controls.h"

//***User Area End->code: Add External Interrupt handler specific headers

/**
   Section: External Interrupt Handlers
 */
/**
  Interrupt Handler for EX_INT1 - INT1
*/
void __attribute__ ( ( interrupt, no_auto_psv ) ) _INT1Interrupt(void)
{
    //***User Area Begin->code: INT1 - External Interrupt 1***
    
    if(DELAY_lockout == 0){
        int prev = VSE_B_prev + VSE_A_prev*2;
        int cur = VSE_B + VSE_A*2;

        if(VEmode){ //0=volume, 1=balance
            processEncoder(prev, cur, -8, 8, &balSEval);
            iflags.b.newbalsel = 1;
        }else{
            processEncoder(prev, cur, 1, 127, &VSEval);
            iflags.b.newse = 1;
        }
        VSE_A_prev = VSE_A;
        VSE_B_prev = VSE_B;
    }else{
        VSE_A_prev = VSE_A;
        VSE_B_prev = VSE_B;
    }
    
    
    
    INTCON2bits.INT1EP = VSE_A;      // toggle interrupt edge polarity (B)

    //***User Area End->code: INT1 - External Interrupt 1***
    EX_INT1_InterruptFlagClear();
}
/**
  Interrupt Handler for EX_INT2 - INT2
*/
void __attribute__ ( ( interrupt, no_auto_psv ) ) _INT2Interrupt(void)
{
    //***User Area Begin->code: INT2 - External Interrupt 2***
    
    if(DELAY_lockout == 0){
        int prev = VSE_B_prev + VSE_A_prev*2;
        int cur = VSE_B + VSE_A*2;

        if(VEmode){ //0=volume, 1=balance
            processEncoder(prev, cur, -8, 8, &balSEval);
            iflags.b.newbalsel = 1;
        }else{
            processEncoder(prev, cur, 1, 127, &VSEval);
            iflags.b.newse = 1;
        }
        VSE_A_prev = VSE_A;
        VSE_B_prev = VSE_B;
    }else{
        VSE_A_prev = VSE_A;
        VSE_B_prev = VSE_B;
    }
    
    
    
    INTCON2bits.INT2EP = VSE_B;		// set interrupt edge (A)

    //***User Area End->code: INT2 - External Interrupt 2***
    EX_INT2_InterruptFlagClear();
}
/**
  Interrupt Handler for EX_INT3 - INT3
*/
void __attribute__ ( ( interrupt, no_auto_psv ) ) _INT3Interrupt(void)
{
    //***User Area Begin->code: INT3 - External Interrupt 3***
    
    

    //***User Area End->code: INT3 - External Interrupt 3***
    EX_INT3_InterruptFlagClear();
}
/**
    Section: External Interrupt Initializers
 */
/**
    void EXT_INT_Initialize(void)

    Initializer for the following external interrupts
    INT1
    INT2
    INT3
*/
void EXT_INT_Initialize(void)
{
    /*******
     * INT1
     * Clear the interrupt flag
     * Set the external interrupt edge detect
     * Enable the interrupt, if enabled in the UI. 
     ********/
    EX_INT1_InterruptFlagClear();   
    EX_INT1_PositiveEdgeSet();
    EX_INT1_InterruptEnable();
    /*******
     * INT2
     * Clear the interrupt flag
     * Set the external interrupt edge detect
     * Enable the interrupt, if enabled in the UI. 
     ********/
    EX_INT2_InterruptFlagClear();   
    EX_INT2_PositiveEdgeSet();
    EX_INT2_InterruptEnable();
    /*******
     * INT3
     * Clear the interrupt flag
     * Set the external interrupt edge detect
     * Enable the interrupt, if enabled in the UI. 
     ********/
    EX_INT3_InterruptFlagClear();   
    EX_INT3_PositiveEdgeSet();
    EX_INT3_InterruptEnable();
}
