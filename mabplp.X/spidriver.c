#if defined(__XC16__)
   #include <xc.h>
#endif

#include "spidriver.h"
#include "mcc_generated_files/spi1.h"
#include "mcc_generated_files/spi2.h"
#include "mcc_generated_files/spi3.h"
#include "globals.h"
#include "macros.h"
#include <math.h>

unsigned int data;

void sendleds(void){
    uint16_t val = 0;
    int i, j;
    for(i=5; i >= 0; i--){
        for(j=0; j<16; j++){
            val += leds[i][j]*pow(2, j);
        }
        data = SPI1_Exchange16bit(val);
        val = 0;
    }
    
    SPI1SS  =  1;
    SPI1SS  =  0; //Latch LED SPI data

}

void sendRelays(void){
    uint16_t val = 0;
    int i, j;
    for(i=4; i >= 0; i--){
        for(j=0; j<8; j++){
            val += relayz[i][j]*pow(2, j);
        }
        data = SPI2_Exchange8bit(val);
        val = 0;
    }
    
    SPI2SS  =  1;
    SPI2SS  =  0; //Latch LED SPI data
}

void sendRelaysVolBal(void){
    uint16_t left = balSEval > 0 ? (VSEval-balSEval) : VSEval; //balance is to the right (balSEval is +) so should reduce volume on the left
    uint16_t right = balSEval < 0 ? (VSEval+balSEval) : VSEval;
    data = SPI3_Exchange8bit(left);
    data = SPI3_Exchange8bit(right);
    SPI3SS = 1;
    SPI3SS = 0;
}