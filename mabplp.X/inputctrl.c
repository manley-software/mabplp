#if defined(__XC16__)
   #include <xc.h>

#endif    
//#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "inputctrl.h"
#include "controls.h"
  
/*
 * Input selector change detected by interrupt.
 * decode input selector encoder position
 */
void newIencoder(void){
    //mute output relays
    RLY_RECMUTE = 0;
    RLY_MUTE = 0;
    sendRelays();
    
    setInputLEDs();
    
    setInputRelays();
    

    hardDelay(500);
    
    //unmute output relays
    RLY_RECMUTE = 1;
    RLY_MUTE = 1;
    sendRelays();
}

void setInputLEDs(void){
    if(GSEval>2){ //gain select is using phono stage
        LED33R = (INSEval == 1); //input 1 LED
        LED34R = (INSEval == 2); //input 2 LED
        LED35R = (INSEval == 3); //input 3 LED
        LED36R = (INSEval == 4); //input 4 LED
        LED37R = (INSEval == 5); //input 5 LED
        LED38R = (INSEval == 6); //input 6 LED
        
        LED33W = 0; //input 1 LED
        LED34W = 0; //input 2 LED
        LED35W = 0; //input 3 LED
        LED36W = 0; //input 4 LED
        LED37W = 0; //input 5 LED
        LED38W = 0; //input 6 LED
    }else{ //gain select is using only line stage
        LED33W = (INSEval == 1); //input 1 LED
        LED34W = (INSEval == 2); //input 2 LED
        LED35W = (INSEval == 3); //input 3 LED
        LED36W = (INSEval == 4); //input 4 LED
        LED37W = (INSEval == 5); //input 5 LED
        LED38W = (INSEval == 6); //input 6 LED
        
        LED33R = 0; //input 1 LED
        LED34R = 0; //input 2 LED
        LED35R = 0; //input 3 LED
        LED36R = 0; //input 4 LED
        LED37R = 0; //input 5 LED
        LED38R = 0; //input 6 LED
    }
    
    sendleds();
}

void setInputRelays(void){
    RLY_IN1 = (INSEval == 1); //input 1 LED
    RLY_IN2 = (INSEval == 2); //input 2 LED
    RLY_IN3 = (INSEval == 3); //input 3 LED
    RLY_IN4 = (INSEval == 4); //input 4 LED
    RLY_IN5 = (INSEval == 5); //input 5 LED
    RLY_IN6 = (INSEval == 6); //input 6 LED
    sendRelays();
}