/* 
 * File:   controls.h
 * Author: Gary-lenovo
 *
 * Created on May 1, 2018, 5:19 PM
 */

#ifndef CONTROLS_H
#define	CONTROLS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    void handleSWTSE(void);
    void handleSWISE(void);
    void handleSWVSE(void);
    void processEncoder(int prev, int cur, int min, int max, volatile char* val);
    void hardDelay(unsigned int ms);


#ifdef	__cplusplus
}
#endif

#endif	/* CONTROLS_H */

