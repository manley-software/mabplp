#if defined(__XC16__)
   #include <xc.h>
#endif

#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "volctrl.h"
#include <math.h>

/*
 * volume display
 * VSEval is ++ or -- by the quad encoder in ext_int.c
 */
void newVencoder(void){
    setVolumeLEDs();
    
    sendRelaysVolBal();
}

void setVolumeLEDs(void){
    LED1 = VSEval > 0;
    LED2 = VSEval > 8;
    LED3 = VSEval > 16;
    LED4 = VSEval > 24;
    LED5 = VSEval > 32;
    LED6 = VSEval > 40;
    LED7 = VSEval > 48;
    LED8 = VSEval > 56;
    LED9 = VSEval > 64;
    LED10 = VSEval > 72;
    LED11 = VSEval > 80;
    LED12 = VSEval > 88;
    LED13 = VSEval > 96;
    LED14 = VSEval > 104;
    LED15 = VSEval > 112;
    LED16 = VSEval > 120;

    sendleds();
}