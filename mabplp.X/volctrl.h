/* 
 * File:   vctl.h
 * Author: Gary-lenovo
 *
 * Created on May 2, 2018, 3:57 PM
 */

#ifndef VCTL_H
#define	VCTL_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    void newVencoder(void);
    void setVolumeLEDs(void);

#ifdef	__cplusplus
}
#endif

#endif	/* VCTL_H */

