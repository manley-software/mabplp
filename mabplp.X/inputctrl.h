#ifndef INPUTCTRL_H
#define	INPUTCTRL_H

#include <xc.h> // include processor files - each processor file is guarded.  

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    void newIencoder(void);
    void setInputLEDs(void);
    void setInputRelays(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

