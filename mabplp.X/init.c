#if defined(__XC16__)
   #include <xc.h>
#endif

#include "globals.h"
#include "spidriver.h"
#include "macros.h"
#include "controls.h"
#include "mcc_generated_files/mcc.h"
#include "loadctrl.h"
#include "volctrl.h"
#include "balctrl.h"
#include "inputctrl.h"
#include "gainctrl.h"


// local declaration 
void init(void);
void ledtest(void);
void relaytest(void);

void init(void){
    
    //initialize front panel LEDs
    PORTDbits.RD9 = 1;
    int i, j;
    for(i=0; i < 5; i++){
        for(j=0; j<16; j++){
            leds[i][j] = 0;
        }
    }
    
    LED33R = INSEval = 1;//set default input select
    sendleds();
    PORTDbits.RD9 = 0;
    
    //initialize relays
    int k, n;
    for(k=0; k < 4; k++){
        for(n=0; j<8; n++){
            relayz[k][n] = 0;
        }
    }
    sendRelays();
    
    //initialize encoder positions
    LSE_A_prev = LSE_A;
    LSE_B_prev = LSE_B;
    ISE_A_prev = ISE_A;
    ISE_B_prev = ISE_B;
    VSE_A_prev = VSE_A;
    VSE_B_prev = VSE_B;
    
    //initialize encoder button states
    SWTSE_btnState = 0;
    SWTSE_prev = SWTSE;
    SWISE_btnState = 0;
    SWISE_prev = SWISE;
    SWVSE_btnState = 0;
    SWVSE_prev = SWVSE;
    
    //initialize input gain select globals and leds
    if(GSEval > 17 || GSEval < 1){
        GSEval = 1;
    }
    if(INSEval > 6 || INSEval < 1){
        INSEval = 1;
    }
    if(INSmode > 1 || INSmode < 0){
        INSmode = 0;
    }
    setGainLEDs();
    setInputLEDs();
    setInputRelays();
    
    //initialize load globals and LEDs
    if(LSEval_R > 33 || LSEval_R < 1){
        LSEval_R = 1;
    }
    if(LSEval_C > 16 || LSEval_C < 1){
        LSEval_C = 1;
    }
    if(LSmode > 1 || LSmode < 0){
        LSmode = 0;
    }
    if(LSmode){
        setRedLoadLEDS(LSEval_C);
    }else{
        setWhiteLoadLEDS(LSEval_R);
    }
    
    //initialize volume globals and LEDs 
    if(VSEval > 127 || VSEval < 0){
        VSEval = 0;
    }
    if(balSEval > 8 || balSEval < -8){
        balSEval = 0;
    }
    if(VEmode > 1 || VEmode < 0){
        VEmode = 0;//0=volume, 1=balance
    }
    if(VEmode){
        setBalanceLEDs();
    }else{
        setVolumeLEDs();
    }
    
    sendleds();
    
    TMR2_Stop();
    TMR2_SoftwareCounterClear();
    TMR3_Stop();
    TMR3_SoftwareCounterClear();
    
    //initialize system timer and counts
    TMR4_Start();
    SWVSE_btn_count = 0;
    
    DELAY_lockout = 0;
}

void ledtest(void){
    int i, j, k, n;
    //i,k is the chip/pin that should be on
    for(i=0; i<5; i++){//set chip number
        for(j=0; j<16; j++){//set pin number
            //cycle through the chips to set the values
            for(k=0; k<5; k++){
                for(n=0; n<16; n++){
                    if(k==i && j==n)
                        leds[k][n] = 1;
                    else
                        leds[k][n] = 0;
                }
            }
            hardDelay(1000);
            PORTDbits.RD9 = 1;
            sendleds();
            PORTDbits.RD9 = 0;
        }
        
    }
}

void relaytest(void){
    int i, j, k, n;
    //i,k is the chip/pin that should be on
    for(i=0; i<4; i++){//set chip number
        for(j=0; j<8; j++){//set pin number
            //cycle through the chips to set the values
            for(k=0; k<4; k++){
                for(n=0; n<8; n++){
                    if(k==i && j==n)
                        relayz[k][n] = 0;
                    else
                        relayz[k][n] = 1;
                }
            }
            
            sendRelays();
            hardDelay(1000);
        }
        
    }
}
