/*
 * file bctl.c  ballance  control
 */

#if defined(__XC16__)
   #include <xc.h>
#endif

#include "mcc_generated_files/mcc.h"
#include "macros.h"
#include "spidriver.h"
#include "globals.h"
#include "balctrl.h"
 
/*
 * Input selector change detected by interrupt.
 * decode input selector encoder position
 */
void newBencoder(void){
    setBalanceLEDs();
    sendRelaysVolBal();
}

void setBalanceLEDs(void){
    LED48W = balSEval == -8;
    LED1 = balSEval == -7;
    LED2 = balSEval == -6;
    LED3 = balSEval == -5;
    LED4 = balSEval == -4;
    LED5 = balSEval == -3;
    LED6 = balSEval == -2;
    LED7 = balSEval == -1;
    LED8 = balSEval == 0;
    LED9 = balSEval == 1;
    LED10 = balSEval == 2;
    LED11 = balSEval == 3;
    LED12 = balSEval == 4;
    LED13 = balSEval == 5;
    LED14 = balSEval == 6;
    LED15 = balSEval == 7;
    LED16 = balSEval == 8;
    LED48R = 0;
    
    sendleds();
}